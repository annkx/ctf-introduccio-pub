<?php
  ini_set("display_errors",1);

  session_start();
  //Afegeix path url com a controlador
  $controller_path = './ctrl'.$_SERVER['REQUEST_URI'].'-ctrl.php';

  //En el cas que no hi hagi path
  if (!file_exists($controller_path)) $controller_path = './ctrl/landing-ctrl.php';

  include $controller_path;


?>

