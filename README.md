# Ctf Introduccio

## Descripció

CTF introducció al reconeixement web i cracking de contrasenyes.

Eines i referènces:

* nikto
* fuff
* crunch
* hashcat
* name-that-hash, hash-identifier, https://hashes.com/en/tools/hash_identifier , https://hashcat.net/wiki/doku.php?id=example_hashes
* hydra
